package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map;

import api.IMovingViolationsManager;
import model.vo.VOMovingViolations;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.Stack;

public class MovingViolationsManager implements IMovingViolationsManager {


	private DoubleLinkedList<VOMovingViolations> accidentList = new DoubleLinkedList<>();

	public void loadInfractions(String movingViolationsFile){

		CSVReader archivo = null;
		try {

			ArrayList <String>accidentes = new ArrayList<String>();
			archivo = new CSVReader(movingViolationsFile);
			archivo.readRecord();
			archivo.readHeaders();
			while (archivo.hasMoreData()){

				String codigo= archivo.getRawRecord();

				String [] valores = codigo.split(",");
				VOMovingViolations nuevo = new VOMovingViolations();
				if (!valores[0].equals("﻿OBJECTID")) {
					nuevo.setObject_id(Integer.parseInt(valores[0]));
					nuevo.setRow_(valores[1]);
					nuevo.setLocation(valores[2]);
					nuevo.setAddress_id(valores[3]);
					nuevo.setStreetsegid(valores[4]);
					nuevo.setXcoord(Double.parseDouble(valores[5]));
					nuevo.setYcoord(Double.parseDouble(valores[6]));
					nuevo.setTickettype(valores[7]);
					nuevo.setFineamt(valores[8]);
					nuevo.setTotalpaid(valores[9]);
					nuevo.setPenalty1(valores[10]);
					nuevo.setPenalty2(valores[11]);
					nuevo.setAccidentindicator(valores[12]);
					nuevo.setTicketissuedate(valores[13]);
					nuevo.setViolationcode(valores[14]);
					nuevo.setViolationdesc(valores[15]);
					accidentList.add(nuevo);
    
				}
				archivo.readRecord();	 
			}




		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	       System.out.println(accidentList.size()+"");

	}
	//1A
	public DoubleLinkedList<VOMovingViolations> verifyObjectIDIsUnique()
	{

		DoubleLinkedList<VOMovingViolations> rta = new DoubleLinkedList<>();
		Map<String, Integer> counts = new HashMap<String, Integer>();
		ListIterator<VOMovingViolations> iter = accidentList.iterator();

		while (iter.hasNext())

		{
			VOMovingViolations current = iter.next();
			if (counts.containsKey(current.getObject_id()+"")&&current.getObject_id()!=0)
			{
				counts.put(current.getObject_id()+"", counts.get(current.getObject_id()+"")+1);


				rta.add(current);
			}
			else {
				counts.put(current.getObject_id()+"",1);
			}



		}

		return rta;

	}
	//2A
	public Queue <VOMovingViolations> infractionsByHour(LocalDateTime startHour, LocalDateTime finishHour)

	{
		Queue <VOMovingViolations> rta = new  Queue<VOMovingViolations>();

		ListIterator<VOMovingViolations> iter = accidentList.iterator();

		while (iter.hasNext())

		{
			VOMovingViolations current = iter.next();
			String fechaC = current.getTicketissuedate();
			if (fechaC.endsWith(".000Z")) {
				fechaC = fechaC.substring(0, fechaC.length() - 5);
			}
			LocalDateTime fecha= convertirFecha_Hora_LDT(fechaC);
			if (current.getTicketissuedate()!=null &&  compareToDate(startHour, fecha)>=-1&&compareToDate(finishHour, fecha)<=1)
			{
				rta.enqueue(current);
			}
		}
		return rta;
	}


	public int compareToDate(LocalDateTime a, LocalDateTime b)
	{ int rta = 0;

	if (a.getMonthValue()<b.getMonthValue())
	{
		return -1;
	} 
	if (a.getMonthValue()>b.getMonthValue())
	{
		return 1;
	} 
	if (a.getDayOfMonth()<b.getDayOfMonth())
	{
		return -1;
	} 
	if (a.getDayOfMonth()>b.getDayOfMonth())
	{
		return 1;
	}   
	if (a.getHour()< b.getHour())
	{
		return -1;
	}

	if (a.getHour()>b.getHour())
	{
		return 1;
	}

	if (a.getMinute()<b.getMinute())
	{
		return  -1;
	}
	if (a.getMinute()>b.getMinute())
	{
		return 1;
	}
	if (a.getSecond()<b.getSecond())
	{
		return -1;
	}

	if (a.getSecond()>b.getSecond())
	{
		return 1;
	}
	else if(a.getHour()==b.getHour()&&a.getMinute()==b.getMinute()&&a.getSecond()==b.getSecond())
	{
		return 0;
	}
	return rta;
	}
	private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora)
	{
		return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"));
	}
	//4A
	public Stack<VOMovingViolations> infractionsByAddress(String address, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {

		Stack <VOMovingViolations> rta = new  Stack<VOMovingViolations>();

		ListIterator<VOMovingViolations> iter = accidentList.iterator();

		while (iter.hasNext())

		{
			VOMovingViolations current = iter.next();
			String fechaC = current.getTicketissuedate();
			if (fechaC.endsWith(".000Z")) {
				fechaC = fechaC.substring(0, fechaC.length() - 5);
			}
			LocalDateTime fecha = convertirFecha_Hora_LDT(fechaC);
			if (current.getTicketissuedate()!=null && current.getAddress_id()!=null&&current.getAddress_id().equals(address)&& compareToDate(fechaInicial, fecha)>=-1&&compareToDate(fechaFinal, fecha)<=1)
			{  
				rta.push(current);
			}

		}
		return sortStack(rta);

	}
	public Stack<VOMovingViolations> sortStack(Stack<VOMovingViolations> input){

		Stack<VOMovingViolations> tmpStack = new Stack<VOMovingViolations>();

		while(!input.isEmpty()) {
			VOMovingViolations tmp = input.pop();

			while(!tmpStack.isEmpty() && Integer.parseInt(tmpStack.peek().getStreetsegid()) > Integer.parseInt(tmp.getStreetsegid())) {
				input.push(tmpStack.pop());
			}
			tmpStack.push(tmp);

		}
		/* Stack<VOMovingViolations> tmpStack = new Stack<VOMovingViolations>();

        while(!input.isEmpty()) {
            VOMovingViolations tmp = input.pop();

            while(!tmpStack.isEmpty() && tmpStack.peek().getObject_id()> tmp.getObject_id()) {
                input.push(tmpStack.pop());
            }
            tmpStack.push(tmp);

        }

        return tmpStack;
		 */
		return tmpStack;
	}

	//3A
	public double[] avg(String violationCode3) {
		ListIterator<VOMovingViolations> iter = accidentList.iterator();
		double[] rta = new double[2];
		while (iter.hasNext())

		{
			VOMovingViolations current = iter.next();

			if (current.getViolationcode().equals(violationCode3)) 
			{
				if (current.getAccidentindicator().equals("Yes"))
				{
					rta[1]+= Double.parseDouble(current.getFineamt());
				}
				else{
					rta[0]+=Double.parseDouble(current.getFineamt());
				}
			}

		}
		return rta;
	} 
	//1C
	public int countMovingViolationsInHourRange(int horaInicia, int horaFinal) {
		int rta =0;


		ListIterator<VOMovingViolations> iter = accidentList.iterator();

		while (iter.hasNext())

		{   

			VOMovingViolations current = iter.next();
			String fechaC = current.getTicketissuedate();
			if (fechaC.endsWith(".000Z")) {
				fechaC = fechaC.substring(0, fechaC.length() - 5);
			}
			LocalDateTime fecha = convertirFecha_Hora_LDT(fechaC);
			if (horaInicia<=fecha.getHour()&&fecha.getHour()<=horaFinal&&horaInicia<=horaFinal)
			{
				rta++;
			}
		}
		return rta;


	}		
	//2C
	public int[] numberPerHour ()
	{ 	int [] rta = new int[24];
        for (int i =0;i<rta.length;i++)
        {
        	rta[i] = countMovingViolationsInHourRange(i, i);
        }
		return rta;	
	}
	//3C 	
	public double totalDebt(LocalDateTime fechaInicial, LocalDateTime fechaFinal)
	{  
		double totalPaid = 0;
		double fineAmt=0;
		double penalty1=0;
		double penalty2=0;
		ListIterator<VOMovingViolations> iter = accidentList.iterator();

		while (iter.hasNext())

		{   

			VOMovingViolations current = iter.next();
			String fechaC = current.getTicketissuedate();
			if (fechaC.endsWith(".000Z")) {
				fechaC = fechaC.substring(0, fechaC.length() - 5);
			}
			LocalDateTime fecha = convertirFecha_Hora_LDT(fechaC);
			if (compareToDate(fechaInicial, fecha)>=-1&&compareToDate(fechaFinal, fecha)<=1)
			{   if (!current.getTotalpaid().isEmpty())
			{
				totalPaid += Double.parseDouble(current.getTotalpaid());
			}
			if (!current.getFineamt().isEmpty())
			{
				fineAmt+= Double.parseDouble(current.getFineamt());
			}
			if (!current.getPenalty1().isEmpty())
			{
				penalty1+=Double.parseDouble(current.getPenalty1());
			}
			if (!current.getPenalty2().isEmpty())
			{ 
				penalty2+=Double.parseDouble(current.getPenalty2());
			}



			}
		}
		return totalPaid-fineAmt-penalty1-penalty2;

	}
	public int totalDebtMonth(int x)
	{  
		int totalPaid = 0;
		int fineAmt=0;
		int penalty1=0;
		int penalty2=0;
		ListIterator<VOMovingViolations> iter = accidentList.iterator();

		while (iter.hasNext())

		{   

			VOMovingViolations current = iter.next();
			String fechaC = current.getTicketissuedate();
			if (fechaC.endsWith(".000Z")) {
				fechaC = fechaC.substring(0, fechaC.length() - 5);
			}
			LocalDateTime fecha = convertirFecha_Hora_LDT(fechaC);
			
			int y =fecha.getMonthValue()%4;
			System.out.println(y+"");
			if (y==x)
			{   if (!current.getTotalpaid().isEmpty())
			{
				totalPaid += Double.parseDouble(current.getTotalpaid());
			}
			if (!current.getFineamt().isEmpty())
			{
				fineAmt+= Double.parseDouble(current.getFineamt());
			}
			if (!current.getPenalty1().isEmpty())
			{
				penalty1+=Double.parseDouble(current.getPenalty1());
			}
			if (!current.getPenalty2().isEmpty())
			{ 
				penalty2+=Double.parseDouble(current.getPenalty2());
			}



			}
		}
		return totalPaid-fineAmt-penalty1-penalty2;

	}
	//4C
	public int[] numberPerMonth ()
	{ 	int [] rta = new int[4];
        for (int i =0;i<rta.length;i++)
        {
        	rta[i] =  totalDebtMonth(i+1);
        }
		return rta;	
   }
}

