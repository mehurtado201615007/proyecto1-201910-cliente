package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;



	public class Queue<T extends Comparable<T>> implements Iterable,IQueue {
	    private int n;         // number of elements on queue
	    private Node first;    // beginning of queue
	    private Node last;     // end of queue

	    // helper linked list class
	    private class Node {
	        private T item;   // the item in the node
	        private Node next;   // reference to next item
	    }

	    /**
	     * Initializes an empty queue.
	     */
	    public Queue() {
	        first = null;
	        last = null;
	        n = 0;
	    }

	    /**
	     * Returns true if this queue is empty.
	     *
	     * @return {@code true} if this queue is empty; {@code false} otherwise
	     */
	    public boolean isEmpty() {
	        return first == null;
	    }

	    /**
	     * Returns the number of items in this queue.
	     *
	     * @return the number of items in this queue
	     */
	    public int size() {
	        return n;
	    }

	    /**
	     * Returns the number of items in this queue.
	     *
	     * @return the number of items in this queue
	     */
	    public int length() {
	        return n;
	    }

	    /**
	     * Returns the item least recently added to this queue.
	     *
	     * @return the item least recently added to this queue
	     * @throws NoSuchElementException if this queue is empty
	     */
	    public T peek() {
	        if (isEmpty()) throw new NoSuchElementException("Queue underflow");
	        return first.item;
	    }

	    /**
	     * Add the item to the queue.
	     */
	    public void enqueue(T item) {
	        Node oldlast = last;
	        last = new Node();
	        last.item = item;
	        last.next = null;
	        if (isEmpty()) first = last;
	        else oldlast.next = last;
	        n++;
	    }

	    /**
	     * Removes and returns the item on this queue that was least recently added.
	     *
	     * @return the item on this queue that was least recently added
	     * @throws NoSuchElementException if this queue is empty
	     */
	    public T dequeue() {
	        if (isEmpty()) throw new NoSuchElementException("Queue underflow");
	        T item = first.item;
	        first = first.next;
	        n--;
	        if (isEmpty()) last = null;   // to avoid loitering
	        return item;
	    }


	    /**
	     * Returns an iterator that iterates over the items in this queue in FIFO order.
	     *
	     * @return an iterator that iterates over the items in this queue in FIFO order
	     */
	    public Iterator<T> iterator() {
	        return new ListIterator();
	    }

	    // an iterator, doesn't implement remove() since it's optional
	    private class ListIterator implements Iterator<T> {
	        private Node current = first;  // node containing current item

	        public boolean hasNext() {
	            return current != null;
	        }

	        public void remove() {
	            throw new UnsupportedOperationException();
	        }

	        public T next() {
	            if (!hasNext()) throw new NoSuchElementException();
	            T item = current.item;
	            current = current.next;
	            return item;
	        }
	    }

		@Override
		public void enqueue(Object t) {
			// TODO Auto-generated method stub
			
		}


	    /**
	     * Unit tests the {@code Queue} data type.
	     */
	}
