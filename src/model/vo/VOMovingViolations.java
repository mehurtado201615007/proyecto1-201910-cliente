package model.vo;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations implements Comparable<VOMovingViolations> {

	private int object_id;

	private String row_;

	private String location;

	private String address_id;

	private String streetsegid;

	private double xcoord;

	private double ycoord;

	private String tickettype;

	private String fineamt;

	private String totalpaid;

	private String penalty1;

	private String penalty2;

	private String accidentindicator;

	private String agencyid;

	private String ticketissuedate;

	private String violationcode;

	private String violationdesc;

	private String row_id;

	public int getObject_id() {
		return object_id;
	}

	public String getRow_() {
		return row_;
	}

	public String getLocation() {
		return location;
	}

	public String getAddress_id() {
		return address_id;
	}

	public String getStreetsegid() {
		return streetsegid;
	}

	public double getXcoord() {
		return xcoord;
	}

	public double getYcoord() {
		return ycoord;
	}

	public String getTickettype() {
		return tickettype;
	}

	public String getFineamt() {
		return fineamt;
	}

	public String getTotalpaid() {
		return totalpaid;
	}

	public String getPenalty1() {
		return penalty1;
	}

	public String getPenalty2() {
		return penalty2;
	}

	public String getAccidentindicator() {
		return accidentindicator;
	}

	public String getAgencyid() {
		return agencyid;
	}

	public String getTicketissuedate() {
		return ticketissuedate;
	}

	public String getViolationcode() {
		return violationcode;
	}

	public String getViolationdesc() {
		return violationdesc;
	}

	public String getRow_id() {
		return row_id;
	}

	public void setObject_id(int object_id) {
		this.object_id = object_id;
	}

	public void setRow_(String row_) {
		this.row_ = row_;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setAddress_id(String address_id) {
		this.address_id = address_id;
	}

	public void setStreetsegid(String streetsegid) {
		this.streetsegid = streetsegid;
	}

	public void setXcoord(double xcoord) {
		this.xcoord = xcoord;
	}

	public void setYcoord(double ycoord) {
		this.ycoord = ycoord;
	}

	public void setTickettype(String tickettype) {
		this.tickettype = tickettype;
	}

	public void setFineamt(String fineamt) {
		this.fineamt = fineamt;
	}

	public void setTotalpaid(String totalpaid) {
		this.totalpaid = totalpaid;
	}

	public void setPenalty1(String penalty1) {
		this.penalty1 = penalty1;
	}

	public void setPenalty2(String penalty2) {
		this.penalty2 = penalty2;
	}

	public void setAccidentindicator(String accidentindicator) {
		this.accidentindicator = accidentindicator;
	}

	public void setAgencyid(String agencyid) {
		this.agencyid = agencyid;
	}

	public void setTicketissuedate(String ticketissuedate) {
		this.ticketissuedate = ticketissuedate;
	}

	public void setViolationcode(String violationcode) {
		this.violationcode = violationcode;
	}

	public void setViolationdesc(String violationdesc) {
		this.violationdesc = violationdesc;
	}

	public void setRow_id(String row_id) {
		this.row_id = row_id;
	}

	@Override
	public int compareTo(VOMovingViolations o) {
		// TODO Auto-generated method stub
		return 0;
	}

}

